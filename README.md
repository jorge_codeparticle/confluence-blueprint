# Confluence Multipage BluePrint

A Confluence Multipage creation tool for Project Management

# Installation Instructions
- run `npm install`
Depending on the time of use, installation may not work on the first try. If it fails, try running `npm update`, then run `npm install` again
- The packages sqlite3 and ngrok do not install on their own. Run these two statements:
  - `npm i sqlite3`
  - `npm i --save-dev ngrok`

- You may have to downgrade to npm v12 and node v6 to install sqllite3!


# The credentials.json file
- If this project does not contain a `credentials.json` file, you need to create one at the root level containing this:
```
{
  "hosts": {
    "yourdomain.atlassian.net": {
      "product": "confluence",
      "username": "youremail",
      "password": "yourapitoken"
    }
  }
}
```
- Make sure you have administrator rights on the space you are going to work on. To get an Api Token, go to
  https://id.atlassian.com/manage/api-tokens

# Creating the pages.json file (or a specific space)
The file `pages.json` is used as a default. Providing the parameter for page file (`REACT_APP_FILE`) will override this use. To
create your file:
  - Make a new file called `anyname.json` at the root level
  - Write down the following hierarchy:
    ```
    {
      "domain": "yourdomain.atlassian.net",
      "space": "YOURSPACE",
      "pages": [
        {
          "title": "Parent 1",
          "content": "Content for parent 1",
          "children": [
            {
              ...
            }
          ]
        }
        ...
      ]
    }
    ```
  - Make sure the `domain` matches the one specified in the file `credentials.json`
  - In order for the program to work, you must first create a space in your Confluence domain. Make sure to write the space `key` (usually a three character long string) in place of `YOURSPACE`
  - The important thing to note is that each object that represents a page needs a `title` and a content` property (the `children` property, though it should allow to create an indefinite number of subpages, is optional). Although the content can be text in HTML format, it may be better to represent this in a single line. There are some tools you can use to help you format this:
    - Word to HTML: https://wordtohtml.net/ . Copy the text you want to convert to HTML here. Depending on the initial formatting of the text, you may experience errors when pasting. Try pasting on a text editor (such as NotePad or open a new file in VSCode with Command + N) first then copying from it and pasting here
    - Remove Line Breaks: http://removelinebreaks.net/ . If you have a lot of content to add to a page, this tool helps to remove/convert line breaks either into spaces or simply get rid of them, to simplify content display in the `pages.json` file
    - Convert Double Quotes to Double Quotes with Dashes and Back: If you have content which needs special formatting (such as coloring), use https://codepen.io/gfcf14/pen/WNpZJyG to paste any text with styles (and thus, double quotes) and click the `CONVERT TO \"` button to convert them from `"` to `\"` which allows for them to be escaped in JSON. Should this text need to be converted back to non-escaped double quotes, paste the escaped text (the one already containing `\"`) to the same text area but this time click to the `CONVERT TO "` button

# Execution Instructions
- You can run the program with `npm start`, with which the file `pages.json` and the space specified in it would be used
- Alternatively, you can specify either `REACT_APP_SPACE` and/or `REACT_APP_FILE` to customize the space to add at and the page files to use
  - `REACT_APP_SPACE=space npm start`
  - `REACT_APP_SPACE=space REACT_APP_FILE=file npm start`

# Troubleshooting
- It is possible, upon running, to get the error "Failed to register with host https:///yourdomain.atlassian.net/wiki"
- Note that the urls are being rendered with three slashes (///)
- To fix, go to `package.json`, and under dependencies add the dependency `"urijs": "1.19.1"`. If there already is a dependency for `urijs`, erase it and add this one
- Quit the program (Ctrl + C), then run `npm install`. This will update the `urijs` package to specifically use the version `1.19.1`. Running `npm start` again should create the pages without issues
- In case you get the following error: `The app host returned HTTP response code 404 when we tried to contact it during installation`, it happens because the installed URL for the add-on, as configured via ngrok, has changed from the one currently running. You must quit the program, remove the add-on from Confluence's installed apps, then run the program again with `npm start`
- You may have to downgrade to npm v12 and node v6 to install sqllite3!
