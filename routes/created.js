const { DOMParser } = require('xmldom');
let xmlserializer = require('xmlserializer');

module.exports = async function (app, addon) {
  app.post('/created', addon.authenticate(), function (req, res) {
    var httpClient = addon.httpClient(req);

    const pageDoc = process.env.REACT_APP_FILE ? require(`../${process.env.REACT_APP_FILE}.json`) : require('../pages.json');
    const spaceKey = process.env.REACT_APP_SPACE ? process.env.REACT_APP_SPACE : pageDoc.space;

    return new Promise(async (resolve, reject) => {
      await postPages(pageDoc.pages, false, httpClient, spaceKey);
      resolve();
    }).then(() => {
      console.log('Confluence Multiple page Blueprint add-on operations have successfully completed');
      process.exit(0);
    });
  });
};

async function postPages(pageList, ancestor, httpClient, spaceKey) {
  for (let i = 0; i < pageList.length; i++) {
    const page = pageList[i];

    const pageId = await pageExistsInSpace(page.title, spaceKey);
    let added = false;

    if (pageId) {
      console.log(`The page ${page.title} already exists in ${spaceKey} with id ${pageId}`);
    } else {
      console.log(`Now trying to add ${page.title} (ancestor: ${ancestor})...`);
      added = await postPage(ancestor, httpClient, page, spaceKey);

      if (added) {
        console.log(`The page ${page.title} was successfully added (id: ${added})`);
      }
    }

    if (page.hasOwnProperty('children')) {
      const parentId = pageId ? pageId : added;
      await postPages(page.children, parentId, httpClient, spaceKey);
    }
  }
}

function getExisting(spaceKey, pageTitle) {
  const credentials = require('../credentials.json');
  const { domain } = process.env.REACT_APP_FILE ? require(`../${process.env.REACT_APP_FILE}.json`) : require('../pages.json');

  const { username, password } = credentials.hosts[domain];

  const encodedTitle = encodeURIComponent(pageTitle);

  return {
    method: 'GET',
    url: `https://${domain}/wiki/rest/api/content?spaceKey=${spaceKey}&title=${encodedTitle}`,
    auth: { username, password },
    headers: {
      'Accept': 'application/json'
    }
  };
}

async function pageExistsInSpace(pageTitle, spaceKey) {
  const request = require('request');

  return new Promise((resolve) => {
    request(getExisting(spaceKey, pageTitle), function (error, response, body) {
      if (error) throw new Error(error);

      const JsonBody = JSON.parse(body);
      const { data : { successful } = { successful: true }, message, results } = JsonBody;

      if (successful) {
        if (!results.length) {
          resolve(null);
        } else {
          const pageId = results[0].id;

          if (pageId) {
            resolve(pageId);
          }

          resolve(null);
        }
      } else {
        console.log(message);
        resolve(null);
      }
    });
  });
}

async function postPage(ancestor, httpClient, page, spaceKey) {
  const { content: pageContent } = page;
  let contentBody = ``;

  if (pageContent) {
    const plainContent = new DOMParser().parseFromString(pageContent, 'text/html');
    contentBody = xmlserializer.serializeToString(plainContent);
  }

  const ancestors = !ancestor ? '' : {
    'ancestors': [{ 'id': ancestor }],
  };

  const content = {
    'type': 'page',
    'title': page.title,
    'space': {
      'key': spaceKey
    },
    'body': {
      'storage': {
        'value': contentBody,
        'representation': 'storage'
      }
    },
    ...ancestors
  };

  return new Promise((resolve) => {
    httpClient.post({
      url: '/rest/api/content',
      headers: {
        'X-Atlassian-Token': 'nocheck'
      },
      json: content
    }, async function (err, res, body) {
      if (!res.body.id) {
        console.log(body.message);
      }
      resolve(res.body.id);
    });
  });
}
